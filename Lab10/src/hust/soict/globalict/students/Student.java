package hust.soict.globalict.students;

import java.text.SimpleDateFormat;

public class Student {
	private int studentID;
	private String studentName;
	private String dob;
	private float gpa;

	public Student() {
	}

	public Student(int studentID, String studentName, String dob, float gpa) {
		this.studentID = studentID;
		this.studentName = studentName;
		this.dob = dob;
		this.gpa = gpa;
	}

	public int getStudentID() {
		return studentID;
	}

	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) throws IllegalDobException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		try {
			this.dob = formatter.format(dob);
		} catch (Exception e) {
			throw new IllegalDobException("invalid DOB");
		}
	}

	public float getGpa() {
		return gpa;
	}

	public void setGpa(float gpa) throws IllegalGPAException {
		if (gpa < 0 || gpa > 10)
			throw new IllegalGPAException();
		this.gpa = gpa;
	}

	@Override
	public String toString() {
		return "Student [studentID=" + studentID + ", studentName=" + studentName + ", dob=" + dob + ", gpa=" + gpa
				+ "]";
	}

}
