package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable, Comparable {

	// ----------------- constructor ----------------------

	public DigitalVideoDisc() {
	}

	public DigitalVideoDisc(int id) {
		super(id);
	}

	public DigitalVideoDisc(String title) {
		super(title);
	}
	
	public DigitalVideoDisc(String title, int length, float cost) {
		super(title, length, cost);
	}

	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}

	public DigitalVideoDisc(String title, String category, String director) {
		super(title, category, director);
	}

	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super(title, category, director, length, cost);
	}

	public DigitalVideoDisc(int id, String title, String category, String director, int length, float cost) {
		super(id, title, category, director, length, cost);
	}

	// ----------------- getter setter ----------------------

	public String getDirectory() {
		return super.getDirector();
	}

	public int getLength() {
		return super.getLength();
	}

	// ----------------- service ----------------------

	@Override
	public String toString() {
		return "DigitalVideoDisc [title = " + getTitle() + ", cost = $" + getCost() + "]";
	}

	// search if a title contains tokens
	public boolean search(String title) {
		title = title.toLowerCase();
		String[] tokens = title.split(" ");
		String media = getTitle().toLowerCase();

		for (int i = 0; i < tokens.length; i++) {
			if (media.indexOf(tokens[i]) == -1)
				return false;
		}
		return true;
	}

	@Override
	public void play() throws PlayerException{
		if (this.getLength() <= 0) {
			System.out.println("ERROR: DVD length is 0");
			throw new PlayerException();
		}
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	
	@Override
	public int compareTo(Object o) {
		DigitalVideoDisc d = (DigitalVideoDisc) o;
//		return getTitle().compareTo(d.getTitle()); // Compare by title
		if (d.getCost() == getCost())
			return 0;
		return getCost() > d.getCost() ? 1 : -1;
	}

}
