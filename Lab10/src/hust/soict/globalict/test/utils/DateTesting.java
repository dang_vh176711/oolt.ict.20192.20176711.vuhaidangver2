package hust.soict.globalict.test.utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTesting {
	public static void main(String[] args) {
		MyDate myDate = new MyDate();
		myDate.accept();
		myDate.print();
		
		myDate.printCurrent();
		
		System.out.println("---------------------------");
		
		MyDate myDate2 = new MyDate("twenty-first", "September", "twenty twenty one");
		myDate2.print();
		
		
		System.out.println("---------------------------");
		
		List<LocalDate> list = new ArrayList<>();

		list.add(LocalDate.of(1999, 8, 14));
		list.add(LocalDate.of(1978, 2, 27));
		list.add(LocalDate.now());
		list.add(LocalDate.of(2012, 4, 24));

		System.out.println("Before sorting");
		list.forEach(System.out::println);
		
		System.out.println("After sorting");
		list = DateUtils.sortDates(list);
		list.forEach(System.out::println);
	}
}
