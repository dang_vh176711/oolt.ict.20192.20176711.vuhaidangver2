package hust.soict.ictglobal.aims;

import hust.soict.ictglobal.aims.media.Book;

public class BookTest {
    public static void main(String[] args) {
        Book book = new Book("Random Paragraph","a paragraph is a group of words put together to form a group that is usually longer than a sentence");
        book.processContent();
        System.out.println("Title: "+book.getTitle());
        System.out.println("Content: "+book.getContent());
        System.out.println("ContentLength: "+book.getContentTokens().size());
        System.out.println("Word frequency: ");
        for (String key: book.getWordFrequency().keySet()) {
            System.out.println(key+" : "+ book.getWordFrequency().get(key));
        }
    }
}
