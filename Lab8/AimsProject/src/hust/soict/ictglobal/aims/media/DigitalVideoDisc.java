package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Disc implements Playable, Comparable {
    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }

    public DigitalVideoDisc(String title, String category, String director) {
        super(title, category, director);
    }

    public DigitalVideoDisc(String title, String category, float cost) {
        super(title, category, cost);
    }

    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());

    }

    @Override
    public int compareTo(Object o) {
        DigitalVideoDisc digitalVideoDisc = (DigitalVideoDisc) o;
        return (int) (this.getCost()-digitalVideoDisc.getCost());
    }
}
