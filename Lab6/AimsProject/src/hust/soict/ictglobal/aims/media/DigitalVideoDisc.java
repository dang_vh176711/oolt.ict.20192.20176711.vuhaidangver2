package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Media {
    private String director;
    private int length;

    public DigitalVideoDisc(String title, String category, String director, int length) {
        super(title, category);
        this.director = director;
        this.length = length;
    }

    public DigitalVideoDisc(int id, String title, String category) {
        super(id, title, category);
    }

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
