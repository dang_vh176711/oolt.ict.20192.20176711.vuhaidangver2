package hust.soict.ictglobal.aims.order;

import hust.soict.ictglobal.aims.media.Media;

import java.util.ArrayList;

public class Order {
    private static int id = 1000;
    private ArrayList<Media> itemsOrdered = new ArrayList<>();

    public int getId() {
        return id;
    }

    public Order() {
    }

    public ArrayList<Media> getItemsOrdered() {
        return itemsOrdered;
    }

    public void setItemsOrdered(ArrayList<Media> itemsOrdered) {
        this.itemsOrdered = itemsOrdered;
    }

    public void addMedia(Media media) {
        this.getItemsOrdered().add(media);
    }

    public void removeMedia(Media media) {
        this.getItemsOrdered().remove(media);
    }

    public float totalCost() {
        float total = 0;
        for (Media media : this.getItemsOrdered()) {
            total += media.getCost();
        }
        return total;
    }
}
