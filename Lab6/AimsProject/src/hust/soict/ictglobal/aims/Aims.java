package hust.soict.ictglobal.aims;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.order.Order;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Aims {
    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void showMiniMenu() {
        System.out.println("Which item you want to add ? ");
        System.out.println("--------------------------------");
        System.out.println("1.Book");
        System.out.println("2.DVD");
        System.out.println("0.Exit");
        System.out.println("--------------------------------");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 1;
        boolean isOption1Chosen = false;
        Order order = new Order();
        while (n > -1 || n < 5) {
            showMenu();
            n = scanner.nextInt();
            if (n == 1) {
                System.out.println("A new order with id = " + order.getId() + " is created");
                isOption1Chosen = true;
            }
            if (n == 2) {
                if (!isOption1Chosen) System.out.println("You must create an order by option 1 first !");
                else {
                    int tempId;
                    int choice = 1;
                    String tempTitle, tempCategory, tempAuthors, tempLength, tempDirector;
                    List<String> authors = new ArrayList<>();
                    while (choice > -1 || choice < 3) {
                        showMiniMenu();
                        choice = scanner.nextInt();
                        scanner.nextLine();
                        if (choice == 1) {
                            System.out.println("Id: ");
                            tempId = Integer.parseInt(scanner.nextLine());
                            System.out.println("Title :");
                            tempTitle = scanner.nextLine();
                            System.out.println("Category: ");
                            tempCategory = scanner.nextLine();
                            System.out.println("Input authors, each one is separated by a comma:");
                            tempAuthors = scanner.nextLine();
                            authors = Arrays.asList(tempAuthors.split(","));
                            Book book = new Book(tempId, tempTitle, tempCategory, authors);

                            order.getItemsOrdered().add(book);
                        }
                        if (choice == 2) {
                            System.out.println("Id: ");
                            tempId = Integer.parseInt(scanner.nextLine());
                            System.out.println("Title :");
                            tempTitle = scanner.nextLine();
                            System.out.println("Category: ");
                            tempCategory = scanner.nextLine();
                            DigitalVideoDisc digitalVideoDisc = new DigitalVideoDisc(tempId, tempTitle, tempCategory);
                            order.getItemsOrdered().add(digitalVideoDisc);
                        }
                        if (choice == 0) break;
                    }

                }

            }
            if (n == 3) {
                if (!isOption1Chosen) System.out.println("You must create an order by option 1 first !");
                else {
                    int tempId, index;
                    if (order.getItemsOrdered().size() != 0) {
                        System.out.println("Input id of item you want to delete :");
                        tempId = scanner.nextInt();
                        for (index = 0; index < order.getItemsOrdered().size(); index++)
                            if (order.getItemsOrdered().get(index).getId() == tempId) break;
                        order.getItemsOrdered().remove(index);
                        System.out.println("Item is already deleted !");
                    } else System.out.println("The item list is empty");
                }
            }
            if (n == 4) {
                if (!isOption1Chosen) System.out.println("You must create an order by option 1 first !");
                else {
                    for (Media media : order.getItemsOrdered()) {
                        System.out.println("Id: " + media.getId());
                        System.out.println("Title: " + media.getTitle());
                        System.out.println("Category: " + media.getCategory());
                        if (media instanceof Book) {
                            System.out.print("Authors: ");
                            for (String auth : ((Book) media).getAuthors()) System.out.println(auth);
                        }
                    }
                }
            }
            if (n == 0) break;
        }
    }
}