package hust.soict.ictglobal.aims;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order;

public class DiskTest {
    public static void main(String[] args) {
        Order anOrder=new Order("3/4/2020");

        DigitalVideoDisc dvd1=new DigitalVideoDisc("The Lion Princess");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        anOrder.addDigitalVideoDisc(dvd1);

        DigitalVideoDisc dvd2=new DigitalVideoDisc("Star Trek");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        anOrder.addDigitalVideoDisc(dvd2);

        DigitalVideoDisc dvd3=new DigitalVideoDisc("Cinderella");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(90);
        anOrder.addDigitalVideoDisc(dvd3);

        System.out.println("Check if there is an item of which title contains The Princess: ");
        for (int i = 0; i <anOrder.getQtyOrdered() ; i++) {
            if(anOrder.getItemOrdered()[i].search("The Princess"))
                System.out.println("Item matched : "+ anOrder.getItemOrdered()[i].getTitle());
        }

        DigitalVideoDisc aDisc =anOrder.getALuckyItem();
        System.out.println("The lucky item today is "+ aDisc.getTitle());
        System.out.print("So the total cost is: ");
        System.out.println(anOrder.totalCost());

    }
}
