package hust.soict.ictglobal.aims;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order;

public class Aims {
    public static void main(String[] args) {
        Order anOrder=new Order("25/3/2020");

        DigitalVideoDisc dvd1=new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        anOrder.addDigitalVideoDisc(dvd1);

        DigitalVideoDisc dvd2=new DigitalVideoDisc("Star Wars");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        anOrder.addDigitalVideoDisc(dvd2);

        DigitalVideoDisc dvd3=new DigitalVideoDisc("Aladdin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(90);
        anOrder.addDigitalVideoDisc(dvd3);

        anOrder.removeDigitalVideoDisc(dvd3);//test remove function


        System.out.print("Total cost is: ");
        System.out.println(anOrder.totalCost());
        anOrder.print(anOrder);

        Order anotherOrder=new Order("15/7/1999");
        anotherOrder.addDigitalVideoDisc(anOrder.getItemOrdered());

        Order otherOrder=new Order("20/4/2020");
        otherOrder.addDigitalVideoDisc(dvd1,dvd2);
        Order anOrder2=new Order("21/12/2005");
        Order anOrder3=new Order("21/12/2005");
        Order anOrder4=new Order("21/12/2005");

    }
}
