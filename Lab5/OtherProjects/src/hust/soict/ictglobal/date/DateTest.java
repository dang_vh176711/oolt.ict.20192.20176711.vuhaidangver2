package hust.soict.ictglobal.date;

public class DateTest {
    public static void main(String[] args) {
        MyDate myDate = new MyDate();
        System.out.println("Create variable myDate belonging to MyDate class");

        System.out.printf("Variable myDate after constructor with no parameters : %d/%d/%d\n", myDate.getDay(), myDate.getMonth(), myDate.getYear());

        myDate = new MyDate(15, 7, 1999);
        System.out.printf("Variable myDate after constructor with 3 parameters : %d/%d/%d\n", myDate.getDay(), myDate.getMonth(), myDate.getYear());

        myDate.setMonth("June");
        System.out.printf("Variable myDate after set month to June : %d/%d/%d\n", myDate.getDay(), myDate.getMonth(), myDate.getYear());

        myDate = new MyDate("February 28th 1990");
        System.out.printf("Variable myDate after constructor with a string : %d/%d/%d\n", myDate.getDay(), myDate.getMonth(), myDate.getYear());

        System.out.println("Test accept function: ");
        myDate.accept();

        System.out.println("Test myPrint() function which print the current hust.soict.ictglobal.date to screen: ");
        myDate.myPrint();

        System.out.println("Test print() function which print the String version of current hust.soict.ictglobal.date :");
        myDate.print();

        System.out.println("Test anotherPrint() function which let the user choose how the hust.soict.ictglobal.date can be printed :");
        myDate.anotherPrint();

        System.out.println("\nTo test DateUtils class, we created an array of 4 MyDate objects ");
        DateUtils dateUtils= new DateUtils();
        MyDate[] myArrayDate=new MyDate[4];
        myArrayDate[0]=new MyDate(15,7,1999);
        myArrayDate[1]=new MyDate(27,9,1972);
        myArrayDate[2]=new MyDate(8,1,1975);
        myArrayDate[3]=new MyDate(7,7,2003);
        DateUtils.sortDates(myArrayDate);
        System.out.println("After calling static sortDates method, the order of the created array is:");
        for (int i = 0; i <myArrayDate.length ; i++) {
            System.out.printf("%d/%d/%d\n",myArrayDate[i].getDay(),myArrayDate[i].getMonth(),myArrayDate[i].getYear());
        }
    }
}
