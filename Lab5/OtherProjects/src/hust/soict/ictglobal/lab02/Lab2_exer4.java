package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Lab2_exer4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your number :");
        int n = scanner.nextInt();
        for (int i = 1; i <= n; i++) {
            for (int j = n; j >=i; j--)
                System.out.print(" ");
            for(int j=1;j<=2*i-1;j++)
                System.out.print("*");
            System.out.println();
        }
    }
}
