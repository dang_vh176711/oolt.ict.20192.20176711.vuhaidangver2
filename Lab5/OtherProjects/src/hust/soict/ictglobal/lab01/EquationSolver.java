package hust.soict.ictglobal.lab01;

import javax.swing.*;

public class EquationSolver {
    public static void main(String[] args) {
        String strNum1, strNum2,strNum3;
        double num1, num2, num3, num4,result;
        JOptionPane.showMessageDialog(null, "The first degree equation with one variable");

        strNum1 = JOptionPane.showInputDialog(null, "Please input the first coefficient: ",
                "Input the first number: ", JOptionPane.INFORMATION_MESSAGE);
        num1 = Double.parseDouble(strNum1);

        strNum2 = JOptionPane.showInputDialog(null, "Please input the second coefficient: ",
                "Input the second number: ", JOptionPane.INFORMATION_MESSAGE);
        num2 = Double.parseDouble(strNum2);
        result=-num2/num1;
        if(num1==0) JOptionPane.showMessageDialog(null, "No root", "Root: ",
                JOptionPane.INFORMATION_MESSAGE);
        else JOptionPane.showMessageDialog(null, result, "Root: ",
                JOptionPane.INFORMATION_MESSAGE);

        JOptionPane.showMessageDialog(null, "The first degree equation with two variables");

        



        JOptionPane.showMessageDialog(null, "The second degree equation with one variable");


        System.exit(0);
    }
}
